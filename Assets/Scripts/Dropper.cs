using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dropper : MonoBehaviour
{

    [SerializeField] float waitingTime = 3f;
    bool dropped = false;
    MeshRenderer renderer;
    Rigidbody rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<MeshRenderer>();
        rigidbody = GetComponent<Rigidbody>();
        renderer.enabled = false;
        rigidbody.useGravity = false;
        // hi
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > waitingTime && !dropped){
            dropped = true;
            renderer.enabled = true;
            rigidbody.useGravity = true;
            Debug.Log(waitingTime + " seconds have past.");
        }
    }
}
