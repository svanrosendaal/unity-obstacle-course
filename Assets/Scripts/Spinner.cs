using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    [SerializeField] float xAngle = 0f;
    [SerializeField] float yAngle = 1f;
    [SerializeField] float zAngle = 0f;
    [SerializeField] float rotateSpeed = 100;

    // Update is called once per frame
    void Update()
    {
        float xSpeed = xAngle * Time.deltaTime * rotateSpeed;
        float ySpeed = yAngle * Time.deltaTime * rotateSpeed;
        float zSpeed = zAngle * Time.deltaTime * rotateSpeed;
        transform.Rotate(xSpeed, ySpeed, zSpeed);
    }
}
